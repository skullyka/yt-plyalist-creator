# yt plyalist creator


## How it works
# Requires oauth authentication!



The script gets its input from input.txt in the following format: 

```
-------------------------------------------
2024-05-24 18:32:23
-------------------------------------------
Tool - Schism
Anathema - Pitiless
Tool - Forty Six & 2
Papa Roach - Broken Home
Dream Theater - The Mirror
Ghost B.C. - Year Zero
```

It saves the process state in a state.json:
```
{"playlist_id": "<playlistid>", "last_processed_index": 6, "input_hash": "<hash of inputfile>"}
```


If quota is out, the next day the script can continue to work.

