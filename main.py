import os
import json
import hashlib
import time
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google.auth.transport.requests import Request
import logging

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


class YouTubePlaylistManager:
    SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']

    def __init__(self, client_secrets_file, token_file, state_file, input_file):
        self.client_secrets_file = client_secrets_file
        self.token_file = token_file
        self.state_file = state_file
        self.input_file = input_file
        self.youtube = self.get_authenticated_service()
        self.state = self.load_state()
        self.input_hash = self.compute_file_hash(input_file)

    def get_authenticated_service(self):
        creds = None
        if os.path.exists(self.token_file):
            creds = Credentials.from_authorized_user_file(self.token_file, self.SCOPES)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(self.client_secrets_file, self.SCOPES)
                flow.redirect_uri = 'http://localhost:8080/'
                creds = flow.run_local_server(port=8080)
            with open(self.token_file, 'w') as token:
                token.write(creds.to_json())
        return build('youtube', 'v3', credentials=creds)

    def create_playlist(self, title, description):
        request_body = {
            'snippet': {
                'title': title,
                'description': description
            },
            'status': {
                'privacyStatus': 'public'  # or 'unlisted' or 'private'
            }
        }
        logging.info(f"Creating playlist with title: {title} and description: {description}")
        try:
            response = self.youtube.playlists().insert(
                part='snippet,status',
                body=request_body
            ).execute()
            playlist_id = response['id']
            logging.info(f"Playlist created with ID: {playlist_id}")
            return playlist_id
        except HttpError as e:
            if e.resp.status == 403 and 'quotaExceeded' in str(e):
                logging.error("Quota exceeded. Exiting.")
                exit(1)
            else:
                logging.error(f"An error occurred: {e}")
                raise e

    def add_video_to_playlist(self, playlist_id, video_id, retries=3):
        request_body = {
            'snippet': {
                'playlistId': playlist_id,
                'resourceId': {
                    'kind': 'youtube#video',
                    'videoId': video_id
                }
            }
        }
        logging.info(f"Adding video ID {video_id} to playlist ID {playlist_id}")
        for attempt in range(retries):
            try:
                response = self.youtube.playlistItems().insert(
                    part='snippet',
                    body=request_body
                ).execute()
                return response
            except HttpError as e:
                if e.resp.status == 403 and 'quotaExceeded' in str(e):
                    logging.error("Quota exceeded. Exiting.")
                    exit(1)
                elif e.resp.status in {500, 503, 409}:  # Retry on server errors or conflicts
                    logging.warning(f"Temporary error occurred: {e}. Retrying in 5 seconds...")
                    time.sleep(5)
                else:
                    logging.error(f"An error occurred: {e}")
                    raise e
        raise Exception(f"Failed to add video ID {video_id} to playlist after {retries} attempts")

    def search_video(self, query):
        logging.info(f"Searching for video with query: {query}")
        try:
            response = self.youtube.search().list(
                part='snippet',
                q=query,
                maxResults=1,
                type='video'
            ).execute()
        except HttpError as e:
            if e.resp.status == 403 and 'quotaExceeded' in str(e):
                logging.error("Quota exceeded. Exiting.")
                exit(1)
            else:
                logging.error(f"An error occurred: {e}")
                raise e

        items = response.get('items')
        if items:
            video = items[0]
            title = video['snippet']['title']
            video_id = video['id']['videoId']
            logging.info(f"Found video: {title} with ID: {video_id}")
            return title, video_id
        return None, None

    def process_input_file_line_by_line(self, start_index):
        with open(self.input_file, 'r') as file:
            for current_index, line in enumerate(file):
                if current_index == 1:
                    date_line = line.strip()
                elif current_index > 1:
                    if current_index <= start_index:
                        continue
                    if line.strip() and not line.startswith('-'):
                        yield current_index, line.strip(), date_line

    def compute_file_hash(self, file_path):
        hasher = hashlib.sha256()
        with open(file_path, 'rb') as f:
            buf = f.read()
            hasher.update(buf)
        return hasher.hexdigest()

    def load_state(self):
        if os.path.exists(self.state_file):
            with open(self.state_file, 'r') as file:
                return json.load(file)
        return None

    def save_state(self, state):
        with open(self.state_file, 'w') as file:
            json.dump(state, file)

    def run(self):
        playlist_id = ""
        last_processed_index = -1

        if self.state:
            if self.state['input_hash'] == self.input_hash:
                playlist_id = self.state['playlist_id']
                last_processed_index = self.state['last_processed_index']
                logging.info(f"Resuming with existing playlist ID: {playlist_id}")
            else:
                logging.info("Input file has changed. Deleting the state file and starting over.")
                os.remove(self.state_file)
                self.state = None

        if not playlist_id:
            date_line = None
            with open(self.input_file, 'r') as file:
                lines = file.readlines()
                if len(lines) > 1:
                    date_line = lines[1].strip()

            if date_line:
                playlist_title = f'kozaka-{date_line.split()[2]}'  # Use the date part for the title
                playlist_description = 'A playlist created by a Python script'
                playlist_id = self.create_playlist(playlist_title, playlist_description)
                self.save_state({'playlist_id': playlist_id, 'last_processed_index': -1, 'input_hash': self.input_hash})

        if not playlist_id:
            logging.error("Failed to create or retrieve playlist ID. Exiting.")
            exit(1)

        for current_index, song, date_line in self.process_input_file_line_by_line(last_processed_index):
            logging.info(f"Processing song: {song}")
            title, video_id = self.search_video(song)
            if video_id:
                try:
                    self.add_video_to_playlist(playlist_id, video_id)
                    logging.info(f'Added {title} to playlist {playlist_id}')
                    last_processed_index = current_index
                    self.save_state({'playlist_id': playlist_id, 'last_processed_index': last_processed_index,
                                     'input_hash': self.input_hash})
                except HttpError as e:
                    logging.error(f'Failed to add {title} to playlist {playlist_id}: {e}')
                    self.save_state({'playlist_id': playlist_id, 'last_processed_index': last_processed_index,
                                     'input_hash': self.input_hash})
                    break  # Break the loop if an error occurs to avoid endless retries
            else:
                logging.warning(f'Could not find video for: {song}')
                self.save_state({'playlist_id': playlist_id, 'last_processed_index': last_processed_index,
                                 'input_hash': self.input_hash})


if __name__ == '__main__':
    client_secrets_file = 'client_secrets.json'
    token_file = 'token.json'
    state_file = 'state.json'
    input_file = 'input.txt'

    manager = YouTubePlaylistManager(client_secrets_file, token_file, state_file, input_file)
    manager.run()
